﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Tools
{
    public enum UserMenu
    {
        View = 1,
        Insert = 2,
        Delete = 3,
        Update = 4,
        Select = 5,
        Share = 6,
        Exit = 7
    }

    public enum AdminMenu
    {
        View = 1,
        Insert = 2,
        Delete = 3,
        Update = 4,
        Select = 5,
        Exit = 6
    }
    public enum PlayListMenu
    {
        Add = 1,
        Delete = 2,
        Share = 3,
        Exit = 4
    }
}
