﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Model
{
    public class Song : BaseModel
    {
        public string Title { get; set; }
        public string ArtistName { get; set; }
        public int Year { get; set; }
    }
}
