﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Model
{
    public class SongPlaylist : BaseModel
    {
        public int PlaylistID { get; set; }
        public int SongID { get; set; }
    }
}
