﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Model
{
    class SharedPlaylist : BaseModel
    {
        public int UserID { get; set; }
        public int PlaylistID { get; set; }
    }
}
