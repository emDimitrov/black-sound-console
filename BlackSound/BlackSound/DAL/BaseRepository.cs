﻿using BlackSound.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.DAL
{
    public abstract class BaseRepository<T> : ICRUDable<T> where T : BaseModel, new()
    {
        protected readonly string _filePath;

        public BaseRepository(string filePath)
        {
            this._filePath = filePath;
        }
        protected abstract void WriteEntity(T entity, StreamWriter writer);
        protected abstract void ReadEntity(T entity, StreamReader reader);
        /*public void ReadEntity(T entity, StreamReader reader)
        {
            string line = reader.ReadLine();
            string[] properties = line.Split(',');
            int propertyNum = 0;
            
            foreach (var pi in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                pi.SetValue(entity, Convert.ChangeType(properties[propertyNum], pi.PropertyType));
                propertyNum++;
                //entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance
            }
        }*/
        /*public void WriteEntity(T entity, StreamWriter writer)
        {

            //pi.SetValue(item, Convert.ChangeType(value, pi.PropertyType), null);
            /*writer.WriteLine("{0},{1},{2},{3},{4}",
                playlist.ID,
                playlist.OwnerID,
                playlist.Name,
                playlist.Description,
                playlist.IsPublic
                );
            int num = 1;
            T item = new T();
            foreach (var pi in entity.GetType().GetProperties())
            {
                int numberOfProps = entity.GetType().GetProperties().Length;
                if (num < numberOfProps)
                {
                    writer.Write(
                        pi.GetValue(entity) + ","
                        );
                    num++;
                }
                else
                {
                    writer.Write(
                        pi.GetValue(entity)
                        );
                }
            }
        }*/
        public void Insert(T entity)
        {
            if (entity.ID < 1)
            {
                entity.ID = GetNextID();
            }
            try
            {
                using (FileStream stream = new FileStream(_filePath, FileMode.Append))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        WriteEntity(entity, writer);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.Error.WriteLine("Invalid data entered. {0}", ex);
            }
        }

        public List<T> GetAll()
        {
            List<T> result = new List<T>();
            try
            {
                using (FileStream stream = new FileStream(_filePath, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        while (!reader.EndOfStream)
                        {
                            T entity = new T();
                            ReadEntity(entity, reader);
                            result.Add(entity);
                        }
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("The file was not found!");
            }

            return result;
        }

        public void Update(T entity)
        {
            try
            {
                int entityId = 0;

                using (FileStream stream = new FileStream(_filePath, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            string[] properties = line.Split(',');
                            if (properties[0] == entity.ID.ToString())
                            {
                                entityId = entity.ID;
                                break;
                            }
                        }
                    }
                }

                Delete(entityId);
                entity.ID = entityId;
                Insert(entity);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("The file was not found!");
            }
        }

        public void Delete(int id)
        {
            try
            {
                string tempFilePath = @"../../DataFiles/temp.txt";
                using (StreamReader sr = new StreamReader(_filePath))
                {
                    using (StreamWriter writer = new StreamWriter(tempFilePath))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string[] properties = line.Split(',');
                            if (properties[0] != id.ToString())
                            {
                                writer.WriteLine(line);
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }

                File.Replace(tempFilePath, _filePath, "backup.txt");
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found!");
            }
        }

        public T GetById(int id)
        {
            try
            {
                using (FileStream stream = new FileStream(_filePath, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        while (!reader.EndOfStream)
                        {
                            T entity = new T();
                            ReadEntity(entity, reader);
                            if (entity.ID == id)
                            {
                                return entity;
                            }
                        }
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("The file was not found!");
            }

            return null;
        }

        public int GetNextID()
        {
            int id = 0;
            int maxId = 0;
            try
            {
                using (StreamReader reader = new StreamReader(_filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        string[] properties = line.Split(',');
                        if (int.TryParse(properties[0], out id))
                        {
                            if (id > maxId)
                            {
                                maxId = id;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return ++maxId;
        }
        public void Save(T entity)
        {
            if (GetById(entity.ID) != null)
            {
                Update(entity);
            }
            else
            {
                Insert(entity);
            }
        }

    }
}
