﻿using BlackSound.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.DAL
{
    class PlaylistRepository : BaseRepository<Playlist>
    {
        public PlaylistRepository(string _filePath)
            : base(_filePath) { }

        protected override void ReadEntity(Playlist playlist, StreamReader reader)
        {
            string line = reader.ReadLine();
            string[] properties = line.Split(',');
            int id = 0;
            if (int.TryParse(properties[0], out id))
            {
                playlist.ID = Int32.Parse(properties[0]);
                playlist.OwnerID = Int32.Parse(properties[1]);
                playlist.Name = properties[2];
                playlist.Description = properties[3];
                playlist.IsPublic = bool.Parse(properties[4]);
            }
        }

        protected override void WriteEntity(Playlist playlist, StreamWriter writer)
        {
            if (playlist.ID < 1)
            {
                playlist.ID = GetNextID();
            }

            try
            {
                writer.WriteLine("{0},{1},{2},{3},{4}",
                    playlist.ID,
                    playlist.OwnerID,
                    playlist.Name,
                    playlist.Description,
                    playlist.IsPublic
                    );
            }
            catch (IOException ex)
            {
                Console.Error.WriteLine("Invalid data entered. {0}", ex);
            }
        }

    }
}
