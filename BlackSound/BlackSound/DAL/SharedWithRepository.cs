﻿using BlackSound.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.DAL
{
    class SharedWithRepository : BaseRepository<SharedPlaylist>
    {
        public SharedWithRepository(string _filePath)
            :base(_filePath) { }

        protected override void ReadEntity(SharedPlaylist playlistSharedWith, StreamReader reader)
        {
            string line = reader.ReadLine();
            string[] properties = line.Split(',');
            int id = 0;
            if (int.TryParse(properties[0], out id))
            {
                playlistSharedWith.ID = Int32.Parse(properties[0]);
                playlistSharedWith.UserID = Int32.Parse(properties[1]);
                playlistSharedWith.PlaylistID = Int32.Parse(properties[2]);
            }
        }
        protected override void WriteEntity(SharedPlaylist playlistSharedWith, StreamWriter writer)
        {
            if (playlistSharedWith.ID < 1)
            {
                playlistSharedWith.ID = GetNextID();
            }

            writer.WriteLine("{0},{1},{2}",
                    playlistSharedWith.ID,
                    playlistSharedWith.UserID,
                    playlistSharedWith.PlaylistID
                    );
        }
        public List<int> GetPlaylistsIds(int userId)
        {
            List<SharedPlaylist> allSharedPlaylists = GetAll();
            List<int> sharedPlaylistsToId = new List<int>();
            for (int i = 0; i < allSharedPlaylists.Count; i++)
            {
                if (allSharedPlaylists[i].UserID == userId)
                {
                    sharedPlaylistsToId.Add(allSharedPlaylists[i].PlaylistID);
                }
            }

            return sharedPlaylistsToId;
        }

        public void DeleteByPlaylistId(int playlistId)
        {
            List<SharedPlaylist> allSharedPlaylists = GetAll();
            foreach (var item in allSharedPlaylists)
            {
                if (item.PlaylistID == playlistId)
                {
                    Delete(item.ID);
                }
            }
        }
    }
}
