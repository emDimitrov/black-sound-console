﻿using BlackSound.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.DAL
{
    public class SongRepository : BaseRepository<Song>
    {
        public SongRepository(string fileName)
            :base(fileName){ }
        
        protected override void ReadEntity(Song song, StreamReader reader)
        {
            string line = reader.ReadLine();
            string[] properties = line.Split(',');
            int id = 0;
            if (int.TryParse(properties[0], out id))
            {
                song.ID = Int32.Parse(properties[0]);
                song.Title = properties[1];
                song.ArtistName = properties[2];
                song.Year = Int32.Parse(properties[3]);
            }
        }

        protected override void WriteEntity(Song song, StreamWriter writer)
        {
            if (song.ID < 1)
            {
                song.ID = GetNextID();
            }

            try
            {
                writer.WriteLine("{0},{1},{2},{3}",
                    song.ID,
                    song.Title,
                    song.ArtistName,
                    song.Year
                    );
            }
            catch (IOException ex)
            {
                Console.Error.WriteLine("Invalid data entered. {0}", ex);
            }
        }
    }
}
