﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.DAL
{
    interface ICRUDable<T>
    {
        void Insert(T entity);

        List<T> GetAll();

        void Update(T entity);

        void Delete(int id);

        int GetNextID();
    }
}
