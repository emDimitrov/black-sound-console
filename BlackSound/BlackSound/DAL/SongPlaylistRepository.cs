﻿using BlackSound.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.DAL
{
    class SongPlaylistRepository : BaseRepository<SongPlaylist>
    {
        public SongPlaylistRepository(string _filePath)
            :base(_filePath) { }
        
        protected override void ReadEntity(SongPlaylist songPlaylist, StreamReader reader)
        {
            string line = reader.ReadLine();
            string[] properties = line.Split(',');
            int id = 0;
            if (int.TryParse(properties[0], out id))
            {
                songPlaylist.ID = Int32.Parse(properties[0]);
                songPlaylist.PlaylistID = Int32.Parse(properties[1]);
                songPlaylist.SongID = Int32.Parse(properties[2]);
            }
        }
        protected override void WriteEntity(SongPlaylist songPlaylist, StreamWriter writer)
        {
            if (songPlaylist.ID < 1)
            {
                songPlaylist.ID = GetNextID();
            }

            writer.WriteLine("{0},{1},{2}",
                    songPlaylist.ID,
                    songPlaylist.PlaylistID,
                    songPlaylist.SongID
                    );
        }

        public void DeleteByPlaylistId(int playlistId)
        {
            List<SongPlaylist> allSongPlaylists = GetAll();
            foreach (var item in allSongPlaylists)
            {
                if (item.PlaylistID == playlistId)
                {
                    Delete(item.ID);
                }
            }
        }
    }
}
