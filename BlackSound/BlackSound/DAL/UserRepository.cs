﻿using BlackSound.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.DAL
{
    class UserRepository : BaseRepository<User>
    {
        public UserRepository(string _filePath)
            : base(_filePath) { }

        public User Authenticate(string username, string password)
        {
            using (StreamReader reader = new StreamReader(this._filePath))
            {
                while (!reader.EndOfStream)
                {
                    User loggedUser = new User();
                    string line = reader.ReadLine();
                    string[] properties = line.Split(',');
                    int id = 0;
                    if (int.TryParse(properties[0], out id))
                    {
                        loggedUser.ID = Int32.Parse(properties[0]);
                        loggedUser.Name = properties[1];
                        loggedUser.Password = properties[2];
                        loggedUser.Email = properties[3];
                        loggedUser.IsAdministrator = bool.Parse(properties[4]);
                    }

                    if (loggedUser.Name == username && loggedUser.Password == password)
                    {
                        return loggedUser;
                    }
                }
            }

            return null;
        }
        protected override void ReadEntity(User user, StreamReader reader)
        {
            string line = reader.ReadLine();
            string[] properties = line.Split(',');
            int id = 0;
            if (int.TryParse(properties[0], out id))
            {
                user.ID = Int32.Parse(properties[0]);
                user.Name = properties[1];
                user.Password = properties[2];
                user.Email = properties[3];
                user.IsAdministrator = bool.Parse(properties[4]);
            }
        }
        protected override void WriteEntity(User entity, StreamWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
