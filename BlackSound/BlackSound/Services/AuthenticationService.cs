﻿using BlackSound.DAL;
using BlackSound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Services
{
    class AuthenticationService
    {
        public static User LoggedUser { get; private set; }

        public static void AuthenticateUser(string username, string password)
        {
            UserRepository userRepo = new UserRepository(@"../../DataFiles/users.txt");
            AuthenticationService.LoggedUser = userRepo.Authenticate(username, password);
        }

    }
}
