﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BlackSound.DAL;
using System.Threading.Tasks;
using BlackSound.Model;
using BlackSound.Services;

namespace BlackSound.View
{
    class LoginView
    {
        public LoginView()
        {
            Console.WriteLine("------------Login------------");

            UserRepository repo = new UserRepository(@"../../DataFiles/users.txt");

            User user;
            while (true)
            {
                Console.Write("Username: ");
                string username = Console.ReadLine();
                Console.Write("Password: ");
                string password = Console.ReadLine();

                AuthenticationService.AuthenticateUser(username, password);
                user = AuthenticationService.LoggedUser;
                if (user != null)
                {
                    Console.Clear();
                    Console.WriteLine("Hello {0}", user.Name);
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Username or Password!");
                    continue;
                }
            }

            if (user.IsAdministrator==true)
            {
                AdminView view = new AdminView();
                view.Show();
            }
            else
            {
                UserView view = new UserView();
                view.Show();
            }

            Console.ReadKey();
        }       
    }
}
