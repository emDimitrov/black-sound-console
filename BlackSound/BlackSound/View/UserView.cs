﻿using BlackSound.DAL;
using BlackSound.Model;
using BlackSound.Tools;
using BlackSound.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.View
{
    class UserView : BaseView<Playlist>
    {
        private int loggedUserId = AuthenticationService.LoggedUser.ID;
        public override BaseRepository<Playlist> GetRepository()
        {
            return new PlaylistRepository(@"../../DataFiles/playlist.txt");
        }
        public void Show()
        {
            while (true)
            {
                UserMenu choice = RenderMenu();
                switch (choice)
                {
                    case UserMenu.View:
                        GetAll();
                        break;
                    case UserMenu.Insert:
                        Insert();
                        break;
                    case UserMenu.Delete:
                        Delete();
                        break;
                    case UserMenu.Update:
                        Update();
                        break;
                    case UserMenu.Select:
                        View();
                        break;
                    case UserMenu.Exit:
                        Exit();
                        break;
                }
            }
        }
        private UserMenu RenderMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("########################################");
                Console.WriteLine("#                                      #");
                Console.WriteLine("#        [G]et all playlist            #");
                Console.WriteLine("#        [V]iew playlist               #");
                Console.WriteLine("#        [A]dd playlist                #");
                Console.WriteLine("#        [U]pdate playlist             #");
                Console.WriteLine("#        [D]elete playlist             #");
                Console.WriteLine("#        E[x]it                        #");
                Console.WriteLine("#                                      #");
                Console.WriteLine("########################################");

                ConsoleKeyInfo input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.G:
                        {
                            return UserMenu.View;
                        }

                    case ConsoleKey.A:
                        {
                            return UserMenu.Insert;
                        }
                    case ConsoleKey.D:
                        {
                            return UserMenu.Delete;
                        }
                    case ConsoleKey.U:
                        {
                            return UserMenu.Update;
                        }
                    case ConsoleKey.V:
                        {
                            return UserMenu.Select;
                        }
                    case ConsoleKey.S:
                        {
                            return UserMenu.Share;
                        }
                    case ConsoleKey.X:
                        {
                            return UserMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }

        private void GetAll()
        {
            Console.Clear();

            SharedWithRepository sharedWithRepo = new SharedWithRepository(@"../../DataFiles/sharedWithPlaylists.txt");
            List<int> sharedPlaylists = sharedWithRepo.GetPlaylistsIds(loggedUserId);

            PlaylistRepository playlistRepository = new PlaylistRepository(@"../../DataFiles/playlist.txt");
            List<Playlist> allPlaylists = playlistRepository.GetAll();
            List<Playlist> userPlaylists = new List<Playlist>();
            List<Playlist> publicPlaylists = new List<Playlist>();
            List<Playlist> sharedToUser = new List<Playlist>();
            foreach (Playlist playlist in allPlaylists)
            {
                if (playlist.IsPublic == true 
                    && playlist.OwnerID!=loggedUserId 
                    && sharedPlaylists.IndexOf(playlist.ID) < 0)
                {
                    publicPlaylists.Add(playlist);
                }
                else if (sharedPlaylists.IndexOf(playlist.ID) >= 0)
                {
                    sharedToUser.Add(playlist);
                }
                else if (playlist.OwnerID==loggedUserId)
                {
                    userPlaylists.Add(playlist);
                }
            }

            Console.WriteLine("-------------Public Playlists------------");
            foreach (var playlist in publicPlaylists)
            {
                ViewEntity(playlist);
            }

            Console.WriteLine("-------------Shared Playlists------------");
            if (sharedToUser.Count==0)
            {
                Console.WriteLine();
                Console.WriteLine("  There are no playlists shared to you");
                Console.WriteLine();
            }
            foreach (var playlist in sharedToUser)
            {
                ViewEntity(playlist);
            }

            Console.WriteLine("-------------Your Playlists--------------");
            foreach (var playlist in userPlaylists)
            {
                ViewEntity(playlist);
            }

            Console.ReadKey(true);
        }

        private void View()
        {
            while (true)
            {
                Console.Clear();
                GetAll();
                Console.Write("Enter playlist ID: ");
                int playlistID = ValidateId();

                PlaylistRepository playlistRepository = new PlaylistRepository(@"../../DataFiles/playlist.txt");

                SharedWithRepository sharedWithRepo = new SharedWithRepository(@"../../DataFiles/sharedWithPlaylists.txt");
                List<int> sharedPlaylistIds = sharedWithRepo.GetPlaylistsIds(loggedUserId);

                Playlist playlist = playlistRepository.GetById(playlistID);
                if (playlist == null)
                {
                    Console.WriteLine("Playlist not found.");
                }
                else if (!playlist.IsPublic && playlist.OwnerID != loggedUserId && sharedPlaylistIds.IndexOf(playlist.ID) < 0)
                {
                    Console.WriteLine("You don't have access to that playlist");
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("-------------Songs in {0} -------------", playlist.Name);

                    List<Song> songsInPlaylist = GetSongs(playlist.ID);
                    foreach (Song song in songsInPlaylist)
                    {
                        Console.WriteLine("Song ID: " + song.ID);
                        Console.WriteLine("Song Title: " + song.Title);
                        Console.WriteLine("Artist Name: " + song.ArtistName);
                        Console.WriteLine("Year: " + song.Year);
                        Console.WriteLine("########################################");
                        Console.WriteLine();
                    }
                    if (playlist.OwnerID == loggedUserId)
                    {
                        PlaylistView playlists = new PlaylistView(playlistID);
                        Console.ReadKey(true);
                        break;
                    }
                    else if (playlistID==1)
                    {
                        Console.ReadKey();
                        break;
                    }
                    else
                    {
                        ShareView sharedPlaylists = new ShareView(playlistID);
                        Console.ReadKey(true);
                        break;
                    }
                }
            }
        }

        private void Delete()
        {
            Console.Clear();
            Console.WriteLine("---------Delete Playlist-----------");
            try
            {
                GetAll();
                Console.Write("Enter ID of the playlist: ");
                int playlistId = ValidateId();

                PlaylistRepository repo = new PlaylistRepository(@"../../DataFiles/playlist.txt");
                Playlist getPlaylist = repo.GetById(playlistId);
                if (getPlaylist != null && getPlaylist.OwnerID == loggedUserId)
                {
                    repo.Delete(playlistId);
                    Console.WriteLine("Playlist deleted successfully");

                    SharedWithRepository sharedRepo = new SharedWithRepository(@"../../DataFiles/sharedWithPlaylists.txt");
                    sharedRepo.DeleteByPlaylistId(playlistId);

                    SongPlaylistRepository songPlaylistRepo = new SongPlaylistRepository(@"../../DataFiles/songPlaylists.txt");
                    songPlaylistRepo.DeleteByPlaylistId(playlistId);
                }
                else if (playlistId == 0)
                {
                    Console.Clear();
                    Console.WriteLine("The playlist doesn't exist.");
                    Console.ReadKey(true);
                }
                else if (getPlaylist.OwnerID != loggedUserId)
                {
                    Console.WriteLine("You can delete only your playlists");
                }
            }
            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("The playlist doesn't exist");
                Console.ReadKey();
            }
        }

        private void Exit()
        {
            Environment.Exit(0);
        }

        public List<Song> GetSongs(int playListId)
        {
            SongPlaylistRepository songPlaylistRepo = new SongPlaylistRepository(@"../../DataFiles/songPlaylists.txt");
            List<SongPlaylist> allSongPlaylists = songPlaylistRepo.GetAll();
            List<int> playlistIds = new List<int>();
            for (int i = 0; i < allSongPlaylists.Count; i++)
            {
                if (allSongPlaylists[i].PlaylistID == playListId)
                {
                    playlistIds.Add(allSongPlaylists[i].SongID);
                }
            }

            SongRepository songRepo = new SongRepository(@"../../DataFiles/songs.txt");
            List<Song> allSongs = songRepo.GetAll();
            List<Song> songs = new List<Song>();
            for (int i = 0; i < allSongs.Count; i++)
            {
                if (playlistIds.IndexOf(allSongs[i].ID) >= 0)
                {
                    songs.Add(allSongs[i]);
                }
            }
            return songs;
        }

    }
}
