﻿using BlackSound.DAL;
using BlackSound.Model;
using BlackSound.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.View
{
    class ShareView : BaseView<SharedPlaylist>
    {
        private int playlistId;
        public ShareView(int playlistId)
        {
            this.playlistId = playlistId;
            Show();
        }
        public override BaseRepository<SharedPlaylist> GetRepository()
        {
            return new SharedWithRepository(@"../../DataFiles/sharedWithPlaylists.txt");
        }

        public void Show()
        {
            while (true)
            {
                PlayListMenu choice = RenderMenu();
                switch (choice)
                {
                    case PlayListMenu.Share:
                        Share();
                        break;
                    case PlayListMenu.Exit:
                        return;
                }
            }
        }
        private PlayListMenu RenderMenu()
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("########################################");
                Console.WriteLine("#                                      #");
                Console.WriteLine("#        [S]hare playlist              #");
                Console.WriteLine("#        E[x]it                        #");
                Console.WriteLine("#                                      #");
                Console.WriteLine("########################################");

                ConsoleKeyInfo input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.S:
                        {
                            return PlayListMenu.Share;
                        }
                    case ConsoleKey.X:
                        {
                            return PlayListMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }
        private void Share()
        {
            Console.WriteLine("--------Share playlist--------");
            Console.Write("Enter user ID you want to share the playlist with: ");

            SharedWithRepository repo = new SharedWithRepository(@"../../DataFiles/sharedWithPlaylists.txt");
            SharedPlaylist share = new SharedPlaylist();
            share.UserID = ValidateId();
            share.PlaylistID = playlistId;
            share.ID = repo.GetNextID();
            repo.Insert(share);
        }
    }
}
