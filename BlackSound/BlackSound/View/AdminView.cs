﻿using BlackSound.DAL;
using BlackSound.Model;
using BlackSound.Tools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.View
{
    class AdminView : BaseView<Song>
    {

        public override BaseRepository<Song> GetRepository()
        {
            return new SongRepository(@"../../DataFiles/songs.txt");
        }
        public void Show()
        {
            while (true)
            {
                AdminMenu choice = RenderMenu();
                switch (choice)
                {
                    case AdminMenu.View:
                        Console.Clear();
                        ViewAll();
                        Console.ReadKey();
                        break;
                    case AdminMenu.Insert:
                        Add();
                        break;
                    case AdminMenu.Delete:
                        Delete();
                        break;
                    case AdminMenu.Update:
                        Update();
                        break;
                    case AdminMenu.Select:
                        Get();
                        break;
                    case AdminMenu.Exit:
                        Exit();
                        break;
                }
            }
        }
        private AdminMenu RenderMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("########################################");
                Console.WriteLine("#                                      #");
                Console.WriteLine("#        [G]et all songs               #");
                Console.WriteLine("#        [V]iew song                   #");
                Console.WriteLine("#        [A]dd song                    #");
                Console.WriteLine("#        [U]pdate song                 #");
                Console.WriteLine("#        [D]elete song                 #");
                Console.WriteLine("#        E[x]it                        #");
                Console.WriteLine("#                                      #");
                Console.WriteLine("########################################");

                ConsoleKeyInfo input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.G:
                        {
                            return AdminMenu.View;
                        }
                    case ConsoleKey.A:
                        {
                            return AdminMenu.Insert;
                        }
                    case ConsoleKey.D:
                        {
                            return AdminMenu.Delete;
                        }
                    case ConsoleKey.U:
                        {
                            return AdminMenu.Update;
                        }
                    case ConsoleKey.V:
                        {
                            return AdminMenu.Select;
                        }
                    case ConsoleKey.X:
                        {
                            return AdminMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }

        private void Add()
        {
            Console.Clear();

            Song song = Insert();

            SongPlaylist allSongs = new SongPlaylist();
            allSongs.PlaylistID = 1;
            allSongs.SongID = song.ID;

            SongPlaylistRepository playlistRepo = new SongPlaylistRepository(@"../../DataFiles/songPlaylists.txt");
            playlistRepo.Insert(allSongs);
        }

        private void Delete()
        {
            int songId = Remove();
            SongPlaylistRepository songPlaylistRepo = new SongPlaylistRepository(@"../../DataFiles/songPlaylists.txt");
            List<SongPlaylist> songInPlaylists = songPlaylistRepo.GetAll();
            for (int i = 0; i < songInPlaylists.Count; i++)
            {
                if (songInPlaylists[i].SongID == songId)
                {
                    songPlaylistRepo.Delete(songInPlaylists[i].ID);
                }
            }
        }

        private void Exit()
        {
            Environment.Exit(0);
        }
    }
}