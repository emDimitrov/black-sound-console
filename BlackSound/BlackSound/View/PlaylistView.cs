﻿using BlackSound.DAL;
using BlackSound.Model;
using BlackSound.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.View
{
    class PlaylistView : BaseView<SongPlaylist>
    {
        private int playlistId;
        public PlaylistView(int playlistId)
        {
            this.playlistId = playlistId;
            Show();
        }

        public override BaseRepository<SongPlaylist> GetRepository()
        {
            return new SongPlaylistRepository(@"../../DataFiles/songPlaylists.txt");
        }

        public void Show()
        {
            while (true)
            {
                PlayListMenu choice = RenderMenu();
                switch (choice)
                {
                    case PlayListMenu.Add:
                        Add();
                        break;
                    case PlayListMenu.Delete:
                        Delete();
                        break;
                    case PlayListMenu.Share:
                        Share();
                        break;
                    case PlayListMenu.Exit:
                        return;
                }

            }
        }

        private PlayListMenu RenderMenu()
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("########################################");
                Console.WriteLine("#                                      #");
                Console.WriteLine("#        [A]dd song to playlist        #");
                Console.WriteLine("#        [D]elete song from playlist   #");
                Console.WriteLine("#        [S]hare playlist              #");
                Console.WriteLine("#        [B]ack to playlist            #");
                Console.WriteLine("#                                      #");
                Console.WriteLine("########################################");

                ConsoleKeyInfo input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.A:
                        {
                            return PlayListMenu.Add;
                        }
                    case ConsoleKey.D:
                        {
                            return PlayListMenu.Delete;
                        }
                    case ConsoleKey.S:
                        {
                            return PlayListMenu.Share;
                        }
                    case ConsoleKey.B:
                        {
                            return PlayListMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }

                }
            }
        }

        private void Add()
        {
            SongPlaylist songPlaylist = new SongPlaylist();
            SongPlaylistRepository songPlaylistRepo = new SongPlaylistRepository(@"../../DataFiles/songPlaylists.txt");

            List<int> songIds = new List<int>();
            List<SongPlaylist> allSongPlaylists = songPlaylistRepo.GetAll();

            for (int i = 0; i < allSongPlaylists.Count; i++)
            {
                songIds.Add(allSongPlaylists[i].SongID);
            }

            Console.WriteLine("Add song to the playlist");
            AdminView adminView = new AdminView();
            adminView.ViewAll();
            Console.Write("Enter Song Id: ");
            int id = ValidateId();

            if (songIds.IndexOf(id) >= 0)
            {
                songPlaylist.SongID = id;
                songPlaylist.ID = songPlaylistRepo.GetNextID();
                songPlaylist.PlaylistID = playlistId;
                songPlaylistRepo.Insert(songPlaylist);
                Console.Clear();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("The song is not found!");
                return;
            }

        }

        private void Delete()
        {
            Console.WriteLine("-------Delete Song from the playlist-------");
            SongPlaylist songPlaylist = new SongPlaylist();

            Console.Write("Enter Song Id: ");
            songPlaylist.SongID = ValidateId();
            songPlaylist.PlaylistID = playlistId;

            SongPlaylistRepository repo = new SongPlaylistRepository(@"../../DataFiles/songPlaylists.txt");
            List<SongPlaylist> playlists = repo.GetAll();
            for (int i = 0; i < playlists.Count; i++)
            {
                if (playlists[i].SongID == songPlaylist.SongID && playlists[i].PlaylistID == songPlaylist.PlaylistID)
                {
                    songPlaylist.ID = playlists[i].ID;
                    repo.Delete(songPlaylist.ID);
                }

            }
        }

        private void Share()
        {
            Console.WriteLine("--------Share playlist--------");
            Console.WriteLine("Enter user ID you want to share the playlist with");

            SharedWithRepository shareRepository = new SharedWithRepository(@"../../DataFiles/sharedWithPlaylists.txt");
            SharedPlaylist share = new SharedPlaylist();
            int userId = ValidateId();
            share.UserID = userId;
            share.PlaylistID = playlistId;

            List<int> sharedPlaylists = shareRepository.GetPlaylistsIds(userId);
            if (sharedPlaylists.IndexOf(playlistId)>=0)
            {
                Console.WriteLine("The playlist is already shared to that user.");
                Console.ReadKey();
                Console.Clear();
            }
            else
            {
                shareRepository.Insert(share);
                Console.WriteLine("The playlist is shared to user {0}", share.UserID);
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
