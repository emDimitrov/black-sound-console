﻿using BlackSound.DAL;
using BlackSound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.View
{
    abstract class BaseView<T> where T : BaseModel, new()
    {
        private List<T> items;
        public BaseView()
        {
            UpdateList();
        }
        public abstract BaseRepository<T> GetRepository();

        public virtual void ViewAll()
        {
            Console.WriteLine();
            Console.WriteLine("=========All " + typeof(T).Name + "s" + "=========");
            foreach (var item in items)
            {
                foreach (var pi in item.GetType().GetProperties())
                {
                    Console.WriteLine("{0}: {1}", pi.Name, pi.GetValue(item));
                }
                Console.WriteLine("========================");
            }
        }

        public void Get()
        {
            Console.Clear();
            int id = ValidateId();
            Console.Clear();
            T item = items.SingleOrDefault(i => i.ID == id);
            if (item == null)
            {
                Console.WriteLine("The {0} was not found!", typeof(T).Name);
                return;
            }
            Console.WriteLine("----------{0} information----------", item.GetType().Name);
            foreach (var pi in item.GetType().GetProperties())
            {
                Console.WriteLine("{0}: {1}", pi.Name, pi.GetValue(item));
            }
            Console.WriteLine("===================================");
            Console.ReadKey(true);
        }

        public T Insert()
        {
            T item = new T();
            Console.WriteLine("---------Add new {0}---------", item.GetType().Name);
            ValidateInput(item);

            GetRepository().Save(item);
            UpdateList();
            Console.WriteLine(" {0} Added Successfully", item.GetType().Name);
            Console.ReadKey(true);
            return item;
        }

        private void ValidateInput(T item)
        {
            foreach (var pi in item.GetType().GetProperties())
            {
                if (pi.PropertyType.ToString() == "System.Boolean")
                {
                    Console.WriteLine();
                    Console.WriteLine("Do you want to make it public?");
                    Console.WriteLine("[Y]es/[N]o");
                    while (true)
                    {
                        ConsoleKeyInfo info = Console.ReadKey();
                        if (info.Key == ConsoleKey.Y)
                        {
                            pi.SetValue(item, Convert.ChangeType(true, pi.PropertyType), null);
                            break;
                        }
                        else if (info.Key == ConsoleKey.N)
                        {
                            pi.SetValue(item, Convert.ChangeType(false, pi.PropertyType), null);
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Y/N are the valid chars");
                        }
                    }
                }
                else if (pi.Name != "ID" && pi.Name != "OwnerID" && pi.Name != "IsPublic")
                {
                    while (true)
                    {
                        Console.Write("\nEnter " + pi.Name + ": ");
                        var value = Console.ReadLine();
                        if (value.Trim() != String.Empty)
                        {
                            pi.SetValue(item, Convert.ChangeType(value, pi.PropertyType), null);
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Enter a valid {0}", pi.Name);
                        }
                    }
                }
                else if (pi.Name == "ID" && item.ID<0)
                {
                    pi.SetValue(item, GetRepository().GetNextID());
                }
                else if (pi.Name == "OwnerID")
                {
                    pi.SetValue(item, Services.AuthenticationService.LoggedUser.ID);
                }
            }
        }

        public virtual void Update()
        {
            T item = new T();
            PropertyInfo prop = item.GetType().GetProperty("ID");
            Console.Write("\nEnter {0} {1}:", item.GetType().Name, prop.Name);
            int itemId = ValidateId();
            var repository = GetRepository();
            item = repository.GetById(itemId);

            if (item == null)
            {
                Console.WriteLine("The {0} does not exist!", typeof(T).Name);
                Console.ReadKey(true);
            }

            ValidateInput(item);
            repository.Save(item);
            UpdateList();

            Console.WriteLine("{0} updated succesfuly!", item.GetType().Name);
            Console.ReadKey(true);
        }

        public int Remove()
        {
            Console.Clear();

            T item = new T();
            Console.WriteLine("----------Delete {0}----------", item.GetType().Name);

            PropertyInfo pi = item.GetType().GetProperty("ID");
            Console.Write("Enter ID of the {0}: ", item.GetType().Name);
            int id = ValidateId();

            var repository = GetRepository();
            item = repository.GetById(id);
            if (item == null)
            {
                Console.Clear();
                Console.WriteLine("The {0} was not found!", typeof(T).Name);

                Console.ReadKey(true);
            }
            else
            {
                repository.Delete(item.ID);
                items.Remove(item);
                UpdateList();
            }

            return id;
        }

        public void UpdateList()
        {
            items = GetRepository().GetAll();
        }

        public int ValidateId()
        {
            int testId = 0;
            int id = 0;

            while (true)
            {
                string isNum = Console.ReadLine();
                if (int.TryParse(isNum, out testId))
                {
                    id = Int32.Parse(isNum);
                    return id;
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Ids contain only numbers");
                }
            }
        }

        public void ViewEntity(T entity)
        {
            foreach (var pi in typeof(T).GetProperties())
            {
                if (pi.Name != "OwnerID")
                {
                    Console.WriteLine("{0}: {1}", pi.Name, pi.GetValue(entity));
                }
            }
            Console.WriteLine("########################################");
            Console.WriteLine();
        }
    }
}

